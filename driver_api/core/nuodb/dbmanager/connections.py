import pynuodb, json

class Connection:
    """NuoDB Databases Connection manager class"""
    def __init__(self, schema, database, host, user, password):
        self.options = {"schema": schema}
        self.connect_args = {'database': database, 'host': host, 'user': user, 'password': password, 'options': self.options}
        self.connection = None

    def start(self):
            self.connection = pynuodb.connect(**self.connect_args)

    def close(self):
            self.connection.close()
