import pynuodb, json

class Query:
    """NuoDB Query for a db"""
    def __init__(self, credentials):
        self.credentials = credentials

    def execute(self, query):
        connection = pynuodb.connect(**self.credentials)
        cursor = connection.cursor()
        cursor.execute(query)
        connection.commit()
        cols = []
        rows = []
        data = []
        if cursor.description != None:
            for col in cursor.description:
                cols.append(col[0])
            for row in cursor.fetchall():
                for x in row:
                    rows.append(x)
        if cursor.description != None:
            i = 0
            value = {}
            for r, row in enumerate(rows):
                if i == len(cols):
                    data.append(value)
                    i = 0
                    value = {}
                value[cols[i]] = row
                i += 1
            data.append(value)

        cursor.close()
        connection.close()
        if data != []:
            return {'cols': cols, 'rows': data}
        return {'cols': [], 'rows': []}
