from pynuodb import entity

class LoginEntity:
    """NuoDB Domain Connection manager class"""
    def __init__(self):
        self.domain = None

    def login(self, user, password):
        self.domain = entity.Domain("localhost", user, password)

    def logout(self):
        self.domain.disconnect()
