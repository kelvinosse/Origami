import json, requests
from flask import Flask, session
from flask_restful import reqparse, abort, Api, Resource
parser = reqparse.RequestParser()
parser.add_argument('username')
parser.add_argument('password')
parser.add_argument('db_name')
parser.add_argument('dba_user')
parser.add_argument('dba_password')

def get_uid(data, hostname):
    for val in data:
        if val['hostname'] == hostname:
            return val['id']
    return "not found"

class Databases(Resource):
    def post(self):
        args = parser.parse_args()
        r = requests.get('http://localhost:8888/api/2/databases', auth=(args['username'], args['password']))
        print r.status_code
        print r.headers['content-type']
        return r.json()

    def put(self):
        try:
            headers = {'content-type': 'application/json'}
            args = parser.parse_args()
            r = requests.get('http://localhost:8888/api/2/hosts', auth=(args['username'], args['password']))
            data = json.loads(r.text)
            uid = get_uid(data['data'], args['username'])
            if uid == 'not found':
                return "Id was not found", 400
            req = { 'name': args['db_name'], 'username': args['dba_user'], 'password': args['dba_password'],
            'template': "Single Host", 'options': {}, "groupOptions": { "SMs": {"mem": "1g"}, "TEs": {"mem": "2g"} },
            "tagConstraints": {}, "variables": { "HOST": uid}}
            db = requests.post('http://localhost:8888/api/2/databases?createTimeout=20000', headers=headers, data=json.dumps(req), auth=(args['username'], args['password']))
        except Exception as e:
            return "An error ocurred: "+str(e)
        return db.json()

    def delete(self):
        try:
            headers = {'content-type': 'application/json'}
            args = parser.parse_args()
            req = {'password': args['password'], 'username': args['username']}
            stopdb = requests.post('http://localhost:8888/api/2/databases/'+args['db_name']+'/stop', data=req, headers=headers, auth=(args['username'], args['password']))
            db = requests.delete('http://localhost:8888/api/2/databases/'+args['db_name'], headers=headers, auth=(args['username'], args['password']))
        except Exception as e:
            return "An error ocurred: "+str(e)
        return db.text
