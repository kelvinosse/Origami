import json
from flask import Flask, session
from flask_restful import reqparse, abort, Api, Resource
from core.nuodb.dbmanager.query import Query
parser = reqparse.RequestParser()
parser.add_argument('schema')
parser.add_argument('database')
parser.add_argument('sql')
parser.add_argument('user')
parser.add_argument('password')


class SQLBus(Resource):
    def post(self):
        try:
            args = parser.parse_args()
            credentials = {'database': args['database'], 'host': "localhost", 'user': args['user'], 'password': args['password'], 'options': {"schema": args['schema']}}
            query = Query(credentials)
            result = query.execute(args['sql'].encode('utf-8'))
        except Exception as e:
            error = "An error ocurred on the query: "+str(e)
            return error, 500
        return result
