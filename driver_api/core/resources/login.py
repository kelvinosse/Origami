import json
from flask import Flask, session
from flask_restful import reqparse, abort, Api, Resource
from core.nuodb.domain.login import LoginEntity
parser = reqparse.RequestParser()
parser.add_argument('username')
parser.add_argument('password')

class Login(Resource):
    def post(self):
        try:
            args = parser.parse_args()
            domain = LoginEntity()
            domain.login(args['username'], args['password'])
            session.permanent = True
            session['domain'] = json.dumps(args)
            domain.logout()
        except Exception as e:
            print e
            return "Invalid login credentials", 401
        return "Welcome :)"

    def delete(self):
        session.pop('domain', None)
        session.pop('user', None)
        return "Logged out"
