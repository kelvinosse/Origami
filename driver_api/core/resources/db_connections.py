import json
from flask import Flask, session
from flask_restful import reqparse, abort, Api, Resource
from core.nuodb.dbmanager.connections import Connection
parser = reqparse.RequestParser()
parser.add_argument('schema')
parser.add_argument('database')
parser.add_argument('host')
parser.add_argument('user')
parser.add_argument('password')

class Connections(Resource):
    def post(self):
        try:
            args = parser.parse_args()
            self.connection = Connection(args['schema'], args['database'], args['host'], args['user'], args['password'])
            self.connection.start()
            session['user'] = json.dumps(self.connection.connect_args)
            self.connection.close()
        except Exception as e:
            print e
            return "Connection error... :(", 500
        return "Connection established... :)"

    def get(self):
        if 'user' in session:
            return session['user']
        return "You must login to db first!", 500

    def delete(self):
        if 'user' in session:
            session.pop('user', None)
            return "Session terminated..."
        return "There's no connection to remove", 500

    def put(self):
        try:
            if 'user' in session:
                args = parser.parse_args()
                session.pop('user', None)
                #Here needs to modify all connection arguments
                self.connection = Connection(args['schema'], args['database'], args['host'], args['user'], args['password'])
                self.connection.start()
                session['user'] = json.dumps(self.connection.connect_args)
                self.connection.close()
            else:
                return "You must login first to edit connections!"
        except Exception as e:
            print e
            return "Connection error...:(", 500
        return "Connection has been updated :)"
