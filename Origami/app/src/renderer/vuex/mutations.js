export default {
  login(state){
    state.pending = true
  },
  login_success(state, credentials){
    state.isLoggedIn = true
    state.pending = false
    state.user = credentials
    state.showDbConnectPrompt = true
    window.localStorage.setItem('db-prompt', true)
    window.localStorage.setItem('user', JSON.stringify(state.user))
  },
  logout(state){
    window.localStorage.removeItem('user')
    window.localStorage.removeItem('db')
    window.localStorage.removeItem('schema')
    window.localStorage.removeItem('db_user')
    window.localStorage.removeItem('db_password')
    state.currentDatabase = ''
    state.currentSchema = ''
    state.db_user = ''
    state.db_password = ''
    state.isLoggedIn = false
  },
  run_query(state, result){
    state.queryResultTable.cols = result.cols
    state.queryResultTable.rows = result.rows
  },
  set_database(state, db){
    state.currentDatabase = db
    window.localStorage.setItem('db', db)
    console.log(state.currentDatabase)
  },
  set_schema(state, schema){
    state.currentSchema = schema
    window.localStorage.setItem('schema', schema)
    console.log(state.currentSchema)
  },
  set_db_credentials(state, data){
    state.db_user = data.db_user
    state.db_password = data.db_password
    window.localStorage.setItem('db_user', data.db_user)
    window.localStorage.setItem('db_password', data.db_password)
  },
  connect_to_db(state, data){
    state.currentDatabase = data.db_name
    state.currentSchema = data.db_schema
    state.db_user = data.db_user
    state.db_password = data.db_password
    window.localStorage.setItem('db', data.db_name)
    window.localStorage.setItem('schema', data.db_schema)
    window.localStorage.setItem('db_user', data.db_user)
    window.localStorage.setItem('db_password', data.db_password)
    state.showDbConnectPrompt = false
    window.localStorage.removeItem('db-prompt')
  },
  fetch_databases(state, payload){
    state.databases = payload
  },
  fetch_schemas(state, payload){
    state.schemas = payload
    state.currentSchema = payload.rows[0].SCHEMA
  },
  fetch_tables(state, payload){

  },
  fetch_indexes(state, payload){

  },
  fetch_indexes(state, payload){

  },
  fetch_fns({commit}, payload){
  },
  fetch_procedures({commit}, payload){
  },
  fetch_triggers({commit}, payload){
  },
  fetch_views(state, payload){
  },
  trigger_db_prompt(state, payload){
    state.showDbConnectPrompt = !state.showDbConnectPrompt
  },
  create_tree_view(state, payload){
    let tree = []
    state.databases.map((val) => {
      tree.push({label: val.name, children: [], type: 'db'})
    })
    state.hierarchyTree = tree
  },
  generate_ddl(state, object){
    state.currentQuery = object
  },
  get_sql(state, object){
    state.currentQuery = object
  },
  sql_statement(state, object){
  },
  create_db(state, payload){

  },
  remove_db(state, payload){
  }
}
