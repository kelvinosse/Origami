import api from './api/api'
import Vue from 'vue'
export default {
  login({ commit }, credentials){
    return new Promise((resolve, reject)=>{
      commit('login')
      let u = {
        username: credentials.name,
        password: credentials.password
      }
      api.post('login', u).then(response => {
        console.log(response.data)
        commit('login_success', credentials)
        resolve(response)
      }).catch(error => {
        reject(error)
      })
    })
  },
  logout({ commit }){
    return new Promise((resolve, reject)=>{
      api.delete('login').then(response => {
        commit('logout')
        console.log(response)
        resolve(response)
      }).catch(error => {
        console.log(error)
        reject(error)
      })
    })
  },
  run_query(state, query){
    return new Promise((resolve, reject)=>{
      let req = {
        "schema": state.getters.currentSchema,
        "database": state.getters.currentDB,
        "sql": query,
        "user": window.localStorage.getItem('db_user'),
        "password": window.localStorage.getItem('db_password')
      }
      console.log("Sending this")
      console.log(JSON.stringify(req))
      api.post('runsql', JSON.stringify(req)).then(response => {
        console.log(response.data.cols)
        state.commit('run_query', response.data)
        resolve(response)
      }).catch(error => {
        console.log(error)
        reject({req})
      })
    })
  },
  set_database({commit}, db){
    commit('set_database', db)
  },
  set_schema({commit}, schema){
    commit('set_schema', schema)
  },
  set_db_credentials({commit}, data){
    commit('set_db_credentials', data)
  },
  connect_to_db(state, data){
    return new Promise((resolve, reject) => {
      let req = {
        schema: "system",
        database: data.db_name,
        host: "localhost",
        user: data.db_user,
        password: data.db_password
      }
      api.post('connect', req).then( res => {
        console.log('Good connection?')
        console.log(res)
        state.commit('connect_to_db', data)
        resolve(data)
      }).catch(error => {
        console.log(error)
        state.showDbConnectPrompt = true
        reject(error)
      })
    })
  },
  fetch_databases({commit}, payload){
    return new Promise((resolve, reject) => {
      let req = {
        username: JSON.parse(window.localStorage.getItem('user')).name,
        password: JSON.parse(window.localStorage.getItem('user')).password
      }
      api.post('databases', req).then(res => {
        console.log('Databases')
        console.log(res.data.data)
        commit('fetch_databases', res.data.data)
        commit('create_tree_view', {})
        resolve(res.data)
      }).catch(error => {
        console.log(error)
        reject(error)
      })
    })
  },
  fetch_schemas({commit}, payload){
    return new Promise((resolve, reject) => {
      let req = {
        user: window.localStorage.getItem('db_user'),
        password: window.localStorage.getItem('db_password'),
        database: window.localStorage.getItem('db')
      }
      api.post('schemas', JSON.stringify(req)).then(res => {
        console.log('Schemas?')
        console.log(res)
        commit('fetch_schemas', res.data)
        resolve(res.data)
      }).catch(error => {
        console.log(error)
        reject(error)
      })
    })
  },
  trigger_db_prompt({commit}, payload){
    commit('trigger_db_prompt', payload)
  },
  create_tree_view({commit}, payload){
    commit('create_tree_view', payload)
  },
  fetch_tables({commit}, payload){
    return new Promise((resolve, reject) => {
      let req = {
        user: window.localStorage.getItem('db_user'),
        password: window.localStorage.getItem('db_password'),
        database: window.localStorage.getItem('db')
      }
      api.post('tables', JSON.stringify(req)).then(res => {
        commit('fetch_tables', res.data)
        resolve(res.data)
      }).catch(error => {
        console.log(error)
        reject(error)
      })
    })
  },
  fetch_indexes({commit}, payload){
    return new Promise((resolve, reject) => {
      let req = {
        user: window.localStorage.getItem('db_user'),
        password: window.localStorage.getItem('db_password'),
        database: window.localStorage.getItem('db')
      }
      api.post('indexes', JSON.stringify(req)).then(res => {
        commit('fetch_indexes', res.data)
        resolve(res.data)
      }).catch(error => {
        console.log(error)
        reject(error)
      })
    })
  },
  fetch_views({commit}, payload){
    return new Promise((resolve, reject) => {
      let req = {
        user: window.localStorage.getItem('db_user'),
        password: window.localStorage.getItem('db_password'),
        database: window.localStorage.getItem('db')
      }
      api.post('views', JSON.stringify(req)).then(res => {
        commit('fetch_views', res.data)
        resolve(res.data)
      }).catch(error => {
        console.log(error)
        reject(error)
      })
    })
  },
  fetch_fns({commit}, payload){
    return new Promise((resolve, reject) => {
      let req = {
        user: window.localStorage.getItem('db_user'),
        password: window.localStorage.getItem('db_password'),
        database: window.localStorage.getItem('db')
      }
      api.post('functions', JSON.stringify(req)).then(res => {
        commit('fetch_fns', res.data)
        resolve(res.data)
      }).catch(error => {
        console.log(error)
        reject(error)
      })
    })
  },
  fetch_procedures({commit}, payload){
    return new Promise((resolve, reject) => {
      let req = {
        user: window.localStorage.getItem('db_user'),
        password: window.localStorage.getItem('db_password'),
        database: window.localStorage.getItem('db')
      }
      api.post('procedures', JSON.stringify(req)).then(res => {
        commit('fetch_procedures', res.data)
        resolve(res.data)
      }).catch(error => {
        console.log(error)
        reject(error)
      })
    })
  },
  fetch_triggers({commit}, payload){
    return new Promise((resolve, reject) => {
      let req = {
        user: window.localStorage.getItem('db_user'),
        password: window.localStorage.getItem('db_password'),
        database: window.localStorage.getItem('db')
      }
      api.post('triggers', JSON.stringify(req)).then(res => {
        commit('fetch_triggers', res.data)
        resolve(res.data)
      }).catch(error => {
        console.log(error)
        reject(error)
      })
    })
  },
  generate_ddl({commit}, object){
    return new Promise((resolve, reject)=> {
      let req = {
      	hostname: "localhost",
      	port: 48004,
        username: JSON.parse(window.localStorage.getItem('user')).name,
        password: JSON.parse(window.localStorage.getItem('user')).password,
      	catalog: window.localStorage.getItem('db'),
      	db_password: window.localStorage.getItem('db_password'),
      	db_user: window.localStorage.getItem('db_user'),
      	object_type: object.type,
      	object_name: object.name,
      	schema: object.schema,
        table: object.table,
        tableNamePattern: object.tableNamePattern
      }
      console.log('This is the request: ')
      console.log(req)
      api.post('getddl', JSON.stringify(req)).then(res => {
        console.log('Good response?')
        console.log(res.data)
        commit('generate_ddl', res.data.data[0].sql)
        resolve(res.data)
      }).catch(error => {
        console.log(error)
        reject(error)
      })
    })
  },
  get_sql({commit}, object){
    return new Promise((resolve, reject)=>{
      let req = {
        type: object.type,
        procedure: object.procedure
      }
      api.post('getsql', req).then(res => {
        console.log('Response?')
        console.log(res.data)
        commit('get_sql', res.data.sql)
        resolve(res.data)
      }).catch(error => {
        console.log(error)
        reject(error)
      })
    })
  },
  sql_statement({commit}, payload){
    return new Promise((resolve, reject)=>{
      let req = {
      	hostname: "localhost",
      	port: 48004,
        username: JSON.parse(window.localStorage.getItem('user')).name,
        password: JSON.parse(window.localStorage.getItem('user')).password,
      	catalog: window.localStorage.getItem('db'),
      	db_password: window.localStorage.getItem('db_password'),
      	db_user: window.localStorage.getItem('db_user'),
      	schema: payload.schema,
        sql: payload.sql
      }
      console.log('Sending this')
      console.log(req)
      api.post('sqlstatement', req).then(res => {
        console.log("is good?")
        console.log(res.data)
        resolve(res.data)
      }).catch(error =>{
        console.log(error)
        reject(error)
      })
    })
  },
  create_db({commit}, payload){
    return new Promise((resolve, reject)=>{
      let req = {
        username: JSON.parse(window.localStorage.getItem('user')).name,
        password: JSON.parse(window.localStorage.getItem('user')).password,
        db_name: payload.name,
        dba_user: payload.user,
        dba_password: payload.password
      }
      console.log('Creating db')
      console.log(req)
      api.put('databases', req).then(res => {
        console.log('is nice?')
        console.log(res.data)
        commit('create_db', res.data)
        resolve(res.data)
      }).catch(error => {
        console.log(error)
        reject(error)
      })
    })
  },
  remove_db({commit}, payload){
    return new Promise((resolve, reject)=>{
      let req = {
        username: JSON.parse(window.localStorage.getItem('user')).name,
        password: JSON.parse(window.localStorage.getItem('user')).password,
        db_name: payload.name
      }
      console.log('Removing db')
      console.log(req)
      api.delete('databases', {data: req, params: { force: true }}).then(res => {
        console.log('is ok uh?')
        console.log(res)
        commit('remove_db', res.data)
        resolve(res.data)
      }).catch(error => {
        console.log(error)
        reject(error)
      })
    })
  }
}
